//#1 OR Operator
	db.users.find(
		{
			$or:[
				{"firstName": {$regex: "A", $options:"i"}},
				{"lastName": {$regex: "A", $options:"i"}}
			]
		},
		{
			"_id": 0,
			"firstName": 1,
			"lastName": 1
		}
	);


//#2
db.users.find(
		{
			$and:[
				{"isAdmin": "yes"},
				{"isActive": "yes"}
			]
		}
	);

//#3
db.courses.find(
		{
			"name": {$regex: "U", $options:"i"},
			"price": {$gte: "13,000.00"}
		}
);